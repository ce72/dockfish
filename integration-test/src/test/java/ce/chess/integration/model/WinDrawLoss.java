package ce.chess.integration.model;

public class WinDrawLoss {
  public int win;
  public int draw;
  public int loss;
}
