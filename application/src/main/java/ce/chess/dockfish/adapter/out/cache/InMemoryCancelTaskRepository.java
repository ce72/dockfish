package ce.chess.dockfish.adapter.out.cache;

import ce.chess.dockfish.domain.model.cancel.CancelTask;
import ce.chess.dockfish.domain.model.task.TaskId;
import ce.chess.dockfish.usecase.out.db.CancelTaskRepository;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import jakarta.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Gauge;

@ApplicationScoped
public class InMemoryCancelTaskRepository implements CancelTaskRepository {

  private final Cache<TaskId, CancelTask> cache =
      CacheBuilder.newBuilder().maximumSize(10).build();

  @Gauge(name = "guava_cache_size", absolute = true, unit = MetricUnits.NONE,
      tags = "cache=InMemoryCancelTaskRepository")
  public long getCacheSize() {
    return cache.size();
  }

  @Override
  public void save(CancelTask task) {
    cache.put(task.getTaskId(), task);
  }

  @Override
  public boolean hasTaskId(TaskId taskId) {
    return cache.asMap().keySet().stream().anyMatch(id -> id.equals(taskId));
  }

}
