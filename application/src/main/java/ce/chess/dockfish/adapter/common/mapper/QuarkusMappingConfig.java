package ce.chess.dockfish.adapter.common.mapper;

import org.mapstruct.MapperConfig;

@MapperConfig(componentModel = "cdi")
interface QuarkusMappingConfig {
}
