package ce.chess.dockfish.adapter.in.rabbit;

import ce.chess.dockfish.adapter.RabbitConfiguration;
import ce.chess.dockfish.domain.model.cancel.CancelTask;
import ce.chess.dockfish.usecase.in.ReceiveCancelTask;

import io.smallrye.reactive.messaging.annotations.Blocking;
import io.vertx.core.json.JsonObject;
import jakarta.enterprise.context.ApplicationScoped;
import java.util.concurrent.CompletionStage;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;

@ApplicationScoped
@Log4j2
@RequiredArgsConstructor
public class CancelTaskConsumer extends AbstractMessageConsumer {

  @NonNull
  private final ReceiveCancelTask service;

  @Incoming(RabbitConfiguration.CHANNEL_CANCEL_TASK_SUBMITTED)
  @Blocking(value = "cancelTask", ordered = false)
  public CompletionStage<Void> consume(Message<JsonObject> message) {
    return super.consumeMessage(message);
  }

  @Override
  protected void handleContent(JsonObject jsonObject) {
    CancelTask request = jsonObject.mapTo(CancelTask.class);
    service.cancelOrSave(request);
  }
}
