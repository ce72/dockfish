package ce.chess.dockfish.usecase.in;

import ce.chess.dockfish.domain.model.result.JobStatus;
import ce.chess.dockfish.domain.model.task.AnalysisRun;
import ce.chess.dockfish.domain.model.task.TaskId;

import java.util.Optional;

public interface QueryAnalysis {
  JobStatus getJobStatus(TaskId taskId);

  Optional<AnalysisRun> getTaskDetails(TaskId taskId);
}
