package ce.chess.dockfish.usecase.out.engine;

public interface ReducePv {
  void reducePvTo(int newPv);
}
