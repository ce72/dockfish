package ce.chess.dockfish.usecase.out.db;

import ce.chess.dockfish.domain.model.cancel.CancelTask;
import ce.chess.dockfish.domain.model.task.TaskId;

public interface CancelTaskRepository {

  void save(CancelTask task);

  boolean hasTaskId(TaskId taskId);
}
