package ce.chess.dockfish.usecase.out.engine;

public interface StartStaticEvaluation {
  String retrieveStaticEvaluation(String fen);
}
