package ce.chess.dockfish.usecase.in;

public interface TerminateAnalysis {
  boolean stop();

  boolean kill();
}
