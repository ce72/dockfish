package ce.chess.dockfish.usecase.in;

import java.util.Set;

public interface QueryConfiguration {
  Set<String> listEngineNames();
}
