package ce.chess.dockfish.usecase.in;

import ce.chess.dockfish.domain.model.cancel.CancelTask;

public interface ReceiveCancelTask {
  void cancelOrSave(CancelTask request);
}
