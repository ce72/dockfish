package ce.chess.dockfish.usecase.in;

import ce.chess.dockfish.domain.model.task.AnalysisRun;
import ce.chess.dockfish.domain.model.task.TaskId;

public interface ReceiveAnalysisRequest {
  TaskId startAsync(AnalysisRun task);

  TaskId startSync(AnalysisRun task);
}
