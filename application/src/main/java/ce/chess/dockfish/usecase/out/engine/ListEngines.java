package ce.chess.dockfish.usecase.out.engine;

import java.util.Set;

public interface ListEngines {
  Set<String> listEngineNames();
}
