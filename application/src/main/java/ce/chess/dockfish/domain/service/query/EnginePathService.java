package ce.chess.dockfish.domain.service.query;

import ce.chess.dockfish.usecase.in.QueryConfiguration;
import ce.chess.dockfish.usecase.out.engine.ListEngines;

import jakarta.enterprise.context.ApplicationScoped;
import java.util.Set;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@ApplicationScoped
@RequiredArgsConstructor
public class EnginePathService implements QueryConfiguration {
  @NonNull
  private final ListEngines listEngines;

  @Override
  public Set<String> listEngineNames() {
    return listEngines.listEngineNames();
  }
}
