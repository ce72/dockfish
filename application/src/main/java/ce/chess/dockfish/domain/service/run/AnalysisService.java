package ce.chess.dockfish.domain.service.run;

import ce.chess.dockfish.domain.model.RejectException;
import ce.chess.dockfish.domain.model.RequeueException;
import ce.chess.dockfish.domain.model.task.AnalysisRun;
import ce.chess.dockfish.domain.model.task.EngineOption;
import ce.chess.dockfish.domain.model.task.TaskId;
import ce.chess.dockfish.domain.service.query.CancelTaskService;
import ce.chess.dockfish.usecase.in.ReceiveAnalysisRequest;
import ce.chess.dockfish.usecase.out.db.TaskRepository;
import ce.chess.dockfish.usecase.out.engine.LockEngine;
import ce.chess.dockfish.usecase.out.engine.RunEngine;

import jakarta.enterprise.context.ApplicationScoped;
import java.time.LocalDateTime;
import java.time.ZoneId;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;
import org.eclipse.microprofile.config.Config;

@ApplicationScoped
@Log4j2
@RequiredArgsConstructor
public class AnalysisService implements ReceiveAnalysisRequest {

  @NonNull
  private final LockEngine lockEngine;

  @NonNull
  private final RunEngine runEngine;

  @NonNull
  private final TaskRepository taskRepository;

  @NonNull
  private final UciOptionsConfiguration uciOptionsConfiguration;

  @NonNull
  private final CancelTaskService cancelTaskService;

  @NonNull
  private final Config config;

  @Override
  public TaskId startAsync(AnalysisRun task) {
    // Use this for rest calls.
    // It will return with a task id immediately and block further analysis requests until finished.
    log.info("Start asynchronous analysis");
    if (!lockEngine.tryAcquireLock()) {
      throw new RequeueException("Task rejected. Engine is still running", task.shortText());
    }
    return startAnalysis(task); // Flow returns. The lock must remain and will be removed by watchdog.
  }

  @Override
  public TaskId startSync(AnalysisRun task) {
    // Use this for messaging.
    // It will block until calculation is finished.
    log.info("Start synchronous analysis");
    if (isTaskAlreadyAnalysed(task)) {
      throw new RejectException("Task has already been analysed.", task.shortText());
    }
    if (TaskId.isPresent(task.taskId()) && cancelTaskService.shouldBeCanceled(task.taskId())) {
      throw new RejectException("Task has been revoked.", task.shortText(), Level.INFO);
    }
    lockEngine.acquireLock();
    TaskId taskId = startAnalysis(task);
    lockEngine.blockWhileActive();
    return taskId;
  }

  private TaskId startAnalysis(AnalysisRun task) {
    try {
      AnalysisRun analysisRun = enrichAnalysisRun(task);

      if (isTaskAlreadyAnalysed(analysisRun)) {
        throw new RejectException("Task has already been analysed.", analysisRun.shortText());
      }
      analysisRun = startEngine(analysisRun);
      taskRepository.save(analysisRun);
      return analysisRun.taskId();
    } catch (Exception ex) { // NOPMD
      lockEngine.releaseLock(); // In case of success the watchdog will remove the lock.
      throw ex;
    }
  }

  private AnalysisRun startEngine(AnalysisRun engineTask) {
    try {
      return runEngine.startAnalysis(engineTask);
    } catch (Exception ex) { // NOPMD
      throw new RequeueException(ex.getMessage(), ex, engineTask.shortText());
    }
  }

  private boolean isTaskAlreadyAnalysed(AnalysisRun task) {
    return taskRepository.hasDuplicate(task);
  }

  private AnalysisRun enrichAnalysisRun(AnalysisRun task) {
    TaskId taskId;
    if (TaskId.isPresent(task.taskId())) {
      taskId = task.taskId();
    } else {
      taskId = TaskId.createNew();
    }
    return task.toBuilder()
        .taskId(taskId)
        .hostname(config.getOptionalValue("hostname", String.class).orElse("hostname"))
        .clearEngineOptions()
        .engineOptions(mergeLocalUciOptions(task).engineOptions())
        .created(LocalDateTime.now(ZoneId.systemDefault()))
        .build();
  }

  private AnalysisRun mergeLocalUciOptions(AnalysisRun task) {
    AnalysisRun result = task;
    for (EngineOption option : uciOptionsConfiguration.getLocalEngineOptions()) {
      log.info("Found custom engine option: {}", option);
      if ("SyzygyPath".equalsIgnoreCase(option.getName())) {
        if (task.useSyzygyPath()) {
          // add SyzygyPath only when Tablebases should be used
          result = result.addOrReplaceOption(option);
        }
      } else {
        result = result.addOrReplaceOption(option);
      }
    }
    return result;
  }
}
