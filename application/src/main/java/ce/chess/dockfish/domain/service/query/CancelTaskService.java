package ce.chess.dockfish.domain.service.query;

import ce.chess.dockfish.domain.model.cancel.CancelTask;
import ce.chess.dockfish.domain.model.result.JobStatus;
import ce.chess.dockfish.domain.model.task.AnalysisRun;
import ce.chess.dockfish.domain.model.task.TaskId;
import ce.chess.dockfish.usecase.in.ReceiveCancelTask;
import ce.chess.dockfish.usecase.out.db.CancelTaskRepository;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@ApplicationScoped
@Log4j2
@RequiredArgsConstructor
public class CancelTaskService implements ReceiveCancelTask {
  @NonNull
  private final AnalysisQueryService analysisQueryService;

  @NonNull
  private final CancelTaskRepository cancelTaskRepository;

  @Override
  public void cancelOrSave(CancelTask request) {
    TaskId taskId = request.getTaskId();
    analysisQueryService.getTaskDetails(taskId).ifPresentOrElse(analysisRun -> {
      if (analysisQueryService.getJobStatus(taskId) == JobStatus.ACTIVE) {
        stop(analysisRun);
      } else {
        log.info("Do not cancel. Is already finished: {}", analysisRun.taskId());
      }
    }, () -> saveTask(request));
  }

  private void saveTask(CancelTask request) {
    log.info("Do not cancel non-running task. Save task");
    cancelTaskRepository.save(request);
  }

  private void stop(AnalysisRun analysisRun) {
    log.info("Cancel analysisRun {}", analysisRun.taskId());
    analysisQueryService.stop();
  }

  public boolean shouldBeCanceled(TaskId taskId) {
    return cancelTaskRepository.hasTaskId(taskId);
  }
}
