package ce.chess.dockfish.domain.service.query;

import ce.chess.dockfish.domain.model.result.JobStatus;
import ce.chess.dockfish.domain.model.task.AnalysisRun;
import ce.chess.dockfish.domain.model.task.TaskId;
import ce.chess.dockfish.usecase.in.QueryAnalysis;
import ce.chess.dockfish.usecase.in.TerminateAnalysis;
import ce.chess.dockfish.usecase.out.db.TaskRepository;
import ce.chess.dockfish.usecase.out.engine.QueryEngine;
import ce.chess.dockfish.usecase.out.engine.RunEngine;

import jakarta.enterprise.context.ApplicationScoped;
import java.util.Optional;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@ApplicationScoped
@Log4j2
@RequiredArgsConstructor
public class AnalysisQueryService implements TerminateAnalysis, QueryAnalysis {

  @NonNull
  private final QueryEngine queryEngine;

  @NonNull
  private final RunEngine runEngine;

  @NonNull
  private final TaskRepository taskRepository;

  @Override
  public JobStatus getJobStatus(TaskId taskId) {
    return queryEngine.getJobStatus(taskId);
  }

  @Override
  public boolean stop() {
    log.info("Stop analysis");
    runEngine.stop();
    return !queryEngine.uciEngineIsRunning();
  }

  @Override
  public boolean kill() {
    log.info("Kill analysis");
    runEngine.kill();
    return !queryEngine.uciEngineIsRunning();
  }

  @Override
  public Optional<AnalysisRun> getTaskDetails(TaskId taskId) {
    return taskRepository.findByTaskId(taskId);
  }
}
