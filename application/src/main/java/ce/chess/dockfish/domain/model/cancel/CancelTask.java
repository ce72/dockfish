package ce.chess.dockfish.domain.model.cancel;

import ce.chess.dockfish.domain.model.task.TaskId;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
@AllArgsConstructor
public class CancelTask {
  String id;

  @JsonIgnore
  public TaskId getTaskId() {
    return new TaskId(id);
  }
}
