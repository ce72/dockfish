package ce.chess.dockfish.domain.model;

import java.io.Serial;
import lombok.Getter;

@Getter
public class RequeueException extends RuntimeException {
  @Serial
  private static final long serialVersionUID = 42L;

  private final String task;

  public RequeueException(String message, Throwable cause, String task) {
    super(message, cause);
    this.task = task;
  }

  public RequeueException(String message, String task) {
    super(message);
    this.task = task;
  }
}
