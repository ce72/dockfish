package ce.chess.dockfish.domain.model.result;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class WinDrawLoss {
  int win;
  int draw;
  int loss;
}
