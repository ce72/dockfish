package ce.chess.dockfish.domain.model;

import java.io.Serial;
import lombok.Getter;
import org.apache.logging.log4j.Level;

@Getter
public class RejectException extends RuntimeException {
  @Serial
  private static final long serialVersionUID = 42L;

  private final String task;

  private final Level level;

  public RejectException(String message, String task) {
    this(message, task, Level.WARN);
  }

  public RejectException(String message, String task, Level level) {
    super(message);
    this.task = task;
    this.level = level;
  }

}
