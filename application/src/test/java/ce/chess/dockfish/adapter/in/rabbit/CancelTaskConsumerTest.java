package ce.chess.dockfish.adapter.in.rabbit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.timeout;

import ce.chess.dockfish.adapter.RabbitConfiguration;
import ce.chess.dockfish.domain.model.cancel.CancelTask;
import ce.chess.dockfish.usecase.in.ReceiveCancelTask;

import io.quarkus.test.InjectMock;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.smallrye.reactive.messaging.memory.InMemoryConnector;
import io.smallrye.reactive.messaging.memory.InMemorySource;
import io.vertx.core.json.JsonObject;
import jakarta.inject.Inject;
import java.util.Map;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.eclipse.microprofile.reactive.messaging.spi.Connector;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.mockito.InOrder;

@QuarkusTest
@Execution(ExecutionMode.SAME_THREAD)
@QuarkusTestResource(InMemoryTestConfig.class)
class CancelTaskConsumerTest {
  private static final CancelTask request = CancelTask.builder().id("id1").build();

  @Inject
  @Connector(InMemoryConnector.CONNECTOR)
  InMemoryConnector connector;

  @InjectMock
  IncomingRabbitMessageConverter incomingRabbitMessageConverter = mock(IncomingRabbitMessageConverter.class);

  @InjectMock
  ReceiveCancelTask service;

  Message<JsonObject> messageMock = spy(Message.class);

  @BeforeEach
  void setUp() {
    JsonObject jsonPayload = JsonObject.mapFrom(request);
    doReturn(
        new IncomingRabbitMessageWrapper("exchange", "routingKey", "correlationId", Map.of(), jsonPayload, false))
        .when(incomingRabbitMessageConverter).convert(any());
    doReturn(jsonPayload).when(messageMock).getPayload();
  }

  @Test
  void whenSendingMessagedThenServiceWillBeCalledAndMessageAcked() {

    InMemorySource<Message<JsonObject>> requestSource =
        connector.source(RabbitConfiguration.CHANNEL_CANCEL_TASK_SUBMITTED);
    requestSource.send(messageMock);

    InOrder inOrder = inOrder(service, messageMock);
    inOrder.verify(service, timeout(1000)).cancelOrSave(request);
    inOrder.verify(messageMock, timeout(100)).ack();
  }


}
