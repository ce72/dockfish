package ce.chess.dockfish.adapter.out.cache;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import ce.chess.dockfish.domain.model.cancel.CancelTask;
import ce.chess.dockfish.domain.model.task.TaskId;

import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class InMemoryCancelTaskRepositoryTest {
  private InMemoryCancelTaskRepository cut;

  @BeforeEach
  void setUp() {
    cut = new InMemoryCancelTaskRepository();

    Stream.of(CancelTask.builder().id("id1").build(),
            CancelTask.builder().id("id2").build(),
            CancelTask.builder().id("id3").build())
        .forEach(cut::save);
    assertThat(cut.getCacheSize(), is(3L));
  }

  @Test
  void findById() {
    assertThat(cut.hasTaskId(new TaskId("id2")), is(true));
  }


}
