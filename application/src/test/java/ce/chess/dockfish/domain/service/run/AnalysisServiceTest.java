package ce.chess.dockfish.domain.service.run;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import ce.chess.dockfish.adapter.common.chess.GamePositionService;
import ce.chess.dockfish.domain.model.RejectException;
import ce.chess.dockfish.domain.model.RequeueException;
import ce.chess.dockfish.domain.model.task.AnalysisRun;
import ce.chess.dockfish.domain.model.task.EngineOption;
import ce.chess.dockfish.domain.model.task.TaskId;
import ce.chess.dockfish.domain.service.query.CancelTaskService;
import ce.chess.dockfish.usecase.out.db.TaskRepository;
import ce.chess.dockfish.usecase.out.engine.LockEngine;
import ce.chess.dockfish.usecase.out.engine.RunEngine;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import org.eclipse.microprofile.config.Config;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.AdditionalAnswers;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AnalysisServiceTest {

  public static final EngineOption LOCAL_OPTION_HASH = new EngineOption("Hash", "HashValue");
  private static final EngineOption CALLER_OPTION_DEFAULT = new EngineOption("name", "value");
  private static final EngineOption CALLER_OPTION_HASH = new EngineOption("Hash", "CallerHashValue");
  public static final EngineOption LOCAL_OPTION_THREADS = new EngineOption("Threads", "ThreadsValue");
  public static final EngineOption LOCAL_OPTION_SYZYGY_PATH = new EngineOption("SyzygyPath", "/syz");

  @Mock
  private LockEngine lockEngine;

  @Mock
  private RunEngine runEngine;

  @Mock
  private TaskRepository taskRepository;

  @Mock
  private UciOptionsConfiguration uciOptionsConfiguration;

  @Mock
  private CancelTaskService cancelTaskService;

  @Mock
  Config config;

  @Captor
  private ArgumentCaptor<AnalysisRun> analysisRequestToController;

  @InjectMocks
  private AnalysisService cut;

  private final AnalysisRun command = AnalysisRun.builder()
      .taskId(new TaskId("id1"))
      .name("someName")
      .engineProgramName("engineId")
      .startingPosition(new GamePositionService().createFrom("1. e4 e5"))
      .initialPv(3)
      .maxDepth(2)
      .engineOptions(List.of(CALLER_OPTION_DEFAULT, CALLER_OPTION_HASH))
      .created(LocalDateTime.now(ZoneId.systemDefault()))
      .build();

  @Nested
  class WhenCalledViaRest {
    @Nested
    class AndEngineIsReady {

      @BeforeEach
      void setUp() {
        given(lockEngine.tryAcquireLock()).willReturn(true);
        given(runEngine.startAnalysis(any())).willAnswer(AdditionalAnswers.returnsFirstArg());
      }

      @Test
      void thenTaskIdIsReturned() {
        TaskId taskId = cut.startAsync(command);

        assertThat(taskId, is(notNullValue()));
      }

      @Test
      void thenControllerIsCalledInOrder() {
        cut.startAsync(command);

        InOrder inOrder = Mockito.inOrder(lockEngine, runEngine, taskRepository);
        inOrder.verify(lockEngine).tryAcquireLock();
        inOrder.verify(runEngine).startAnalysis(analysisRequestToController.capture());
        inOrder.verify(taskRepository).save(analysisRequestToController.getValue());
      }

      @Test
      void thenControllerIsCalledWithCorrectAnalysisRequest() {
        TaskId taskId = cut.startAsync(command);

        verify(runEngine).startAnalysis(analysisRequestToController.capture());
        AnalysisRun actualAnalysisRequest = analysisRequestToController.getValue();
        assertThat(actualAnalysisRequest.taskId(), is(equalTo(taskId)));
        assertThat(actualAnalysisRequest.initialPv(), is(equalTo(command.initialPv())));
        assertThat(actualAnalysisRequest.maxDepth(), is(equalTo(command.maxDepth())));
        assertThat(actualAnalysisRequest.engineOptions(), contains(CALLER_OPTION_DEFAULT, CALLER_OPTION_HASH));
        assertThat(actualAnalysisRequest.uciEngineName(), is(Optional.empty()));
      }

      @Test
      void doesReleaseLockAfterError() {
        given(runEngine.startAnalysis(any()))
            .willThrow(IllegalArgumentException.class);

        assertThrows(RequeueException.class, () -> cut.startAsync(command));

        verify(lockEngine).releaseLock();
      }

      @Test
      void whenCalledWithGivenTaskIdThenReturnTaskId() {
        given(lockEngine.tryAcquireLock()).willReturn(true);
        TaskId taskId = new TaskId("TASK_ID");
        AnalysisRun withTaskId = command.toBuilder()
            .taskId(taskId)
            .build();

        TaskId actual = cut.startAsync(withTaskId);

        assertThat(actual, is(equalTo(taskId)));
      }

      @Nested
      class GivenLocalOptions {
        @BeforeEach
        void setUp() {
          given(uciOptionsConfiguration.getLocalEngineOptions())
              .willReturn(List.of(LOCAL_OPTION_HASH, LOCAL_OPTION_THREADS, LOCAL_OPTION_SYZYGY_PATH));
        }

        @Test
        void thenDelegateWithMergedOptions() {
          given(lockEngine.tryAcquireLock()).willReturn(true);

          TaskId taskId = cut.startAsync(command);

          assertThat(taskId, is(notNullValue()));
          verify(runEngine).startAnalysis(analysisRequestToController.capture());
          assertThat(analysisRequestToController.getValue().engineOptions(),
              containsInAnyOrder(CALLER_OPTION_DEFAULT, LOCAL_OPTION_HASH, LOCAL_OPTION_THREADS));
        }

        @Test
        void thenDelegateWithMergedOptionsWithSyzygyPath() {
          given(lockEngine.tryAcquireLock()).willReturn(true);

          TaskId taskId = cut.startAsync(command.toBuilder().useSyzygyPath(true).build());

          assertThat(taskId, is(notNullValue()));
          verify(runEngine).startAnalysis(analysisRequestToController.capture());
          assertThat(analysisRequestToController.getValue().engineOptions(),
              containsInAnyOrder(CALLER_OPTION_DEFAULT, LOCAL_OPTION_HASH, LOCAL_OPTION_THREADS,
                  LOCAL_OPTION_SYZYGY_PATH));
        }
      }

    }

    @Nested
    class ButEngineIsAlreadyRunning {

      @BeforeEach
      void setUp() {
        given(lockEngine.tryAcquireLock()).willReturn(false);
      }

      @Test
      void thenThrowRequeueException() {

        assertThrows(RequeueException.class, () -> cut.startAsync(mock(AnalysisRun.class)));

        verify(lockEngine).tryAcquireLock();
        verifyNoMoreInteractions(lockEngine);
        verifyNoMoreInteractions(runEngine);
      }
    }

    @Nested
    class ButTaskIsDuplicate {

      @BeforeEach
      void setUp() {
        given(lockEngine.tryAcquireLock()).willReturn(true);
        given(taskRepository.hasDuplicate(any())).willReturn(true);
      }

      @Test
      void thenThrowRejectException() {

        assertThrows(RejectException.class, () -> cut.startAsync(command));

        verify(lockEngine).tryAcquireLock();
        verify(lockEngine).releaseLock();
        verifyNoMoreInteractions(runEngine);
      }
    }

  }

  @Nested
  class WhenCalledViaMessaging {

    @Nested
    class AndTaskIsNotDuplicate {
      @Test
      void thenDelegateToController() {
        given(cancelTaskService.shouldBeCanceled(new TaskId("id1"))).willReturn(false);
        given(runEngine.startAnalysis(any(AnalysisRun.class))).willAnswer(AdditionalAnswers.returnsFirstArg());
        given(config.getOptionalValue("hostname", String.class)).willReturn(Optional.of("testhost"));

        TaskId taskId = cut.startSync(command);

        assertThat(taskId, is(notNullValue()));

        InOrder inOrder = Mockito.inOrder(lockEngine, runEngine, taskRepository);
        inOrder.verify(lockEngine).acquireLock();
        inOrder.verify(runEngine).startAnalysis(analysisRequestToController.capture());
        inOrder.verify(taskRepository).save(analysisRequestToController.getValue());
        inOrder.verify(lockEngine).blockWhileActive();
        assertThat(analysisRequestToController.getValue().taskId(), is(equalTo(taskId)));
        assertThat(analysisRequestToController.getValue().initialPv(), is(equalTo(command.initialPv())));
        assertThat(analysisRequestToController.getValue().maxDepth(), is(equalTo(command.maxDepth())));
        assertThat(analysisRequestToController.getValue().engineOptions(), is(equalTo(command.engineOptions())));
        assertThat(analysisRequestToController.getValue().uciEngineName(), is(Optional.empty()));
        assertThat(analysisRequestToController.getValue().hostname(), is("testhost"));
      }

      @Test
      void doesReleaseLockAfterError() {
        given(runEngine.startAnalysis(any()))
            .willThrow(IllegalArgumentException.class);

        assertThrows(RequeueException.class, () -> cut.startSync(command));

        verify(lockEngine).releaseLock();
      }
    }

    @Nested
    class ButTaskIsKnown {

      @BeforeEach
      void setUp() {
        given(taskRepository.hasDuplicate(any())).willReturn(true);
      }

      @Test
      void thenThrowRejectException() {

        assertThrows(RejectException.class, () -> cut.startSync(command));

        verifyNoInteractions(lockEngine);
        verifyNoInteractions(runEngine);
      }
    }

    @Nested
    class ButTaskWasCanceled {

      @BeforeEach
      void setUp() {
        given(cancelTaskService.shouldBeCanceled(new TaskId("id1"))).willReturn(true);
      }

      @Test
      void thenThrowRejectException() {

        assertThrows(RejectException.class, () -> cut.startSync(command));

        verifyNoInteractions(lockEngine);
        verifyNoInteractions(runEngine);
      }
    }
  }

}
