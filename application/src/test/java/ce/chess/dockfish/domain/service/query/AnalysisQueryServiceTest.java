package ce.chess.dockfish.domain.service.query;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import ce.chess.dockfish.domain.model.task.AnalysisRun;
import ce.chess.dockfish.domain.model.task.TaskId;
import ce.chess.dockfish.usecase.out.db.TaskRepository;
import ce.chess.dockfish.usecase.out.engine.QueryEngine;
import ce.chess.dockfish.usecase.out.engine.RunEngine;

import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AnalysisQueryServiceTest {

  @Mock
  private QueryEngine queryEngine;

  @Mock
  private RunEngine runEngine;

  @Mock
  private TaskRepository taskRepository;

  @InjectMocks
  private AnalysisQueryService cut;

  @Test
  void stopAnalysisDelegatesToEngineController() {
    given(queryEngine.uciEngineIsRunning()).willReturn(false);

    boolean result = cut.stop();

    verify(runEngine).stop();
    assertThat(result, is(true));
  }

  @Test
  void killAnalysisDelegatesToEngineController() {
    given(queryEngine.uciEngineIsRunning()).willReturn(false);

    boolean result = cut.kill();

    verify(runEngine).kill();
    assertThat(result, is(true));
  }

  @Test
  void getTaskDetailsDelegatesToRepository() {
    TaskId taskId = new TaskId("42");
    AnalysisRun analysisRun = mock(AnalysisRun.class);
    given(taskRepository.findByTaskId(taskId)).willReturn(Optional.of(analysisRun));

    AnalysisRun taskDetails = cut.getTaskDetails(taskId).orElseThrow();

    verify(taskRepository).findByTaskId(taskId);
    assertThat(taskDetails, is(equalTo(analysisRun)));
  }

}
