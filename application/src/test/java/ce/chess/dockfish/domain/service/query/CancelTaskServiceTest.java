package ce.chess.dockfish.domain.service.query;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import ce.chess.dockfish.domain.model.cancel.CancelTask;
import ce.chess.dockfish.domain.model.result.JobStatus;
import ce.chess.dockfish.domain.model.task.AnalysisRun;
import ce.chess.dockfish.domain.model.task.TaskId;
import ce.chess.dockfish.usecase.out.db.CancelTaskRepository;

import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CancelTaskServiceTest {
  private static final TaskId TASK_ID = new TaskId("id1");
  private static final CancelTask cancelTaskRequest = CancelTask.builder().id("id1").build();

  @Mock
  private AnalysisQueryService analysisQueryService;

  @Mock
  private CancelTaskRepository cancelTaskRepository;

  @InjectMocks
  private CancelTaskService cut;

  @Mock
  private AnalysisRun analysisRun;

  @Nested
  class GivenAnalysisIsRunning {
    @BeforeEach
    void setUp() {
      given(analysisQueryService.getTaskDetails(TASK_ID)).willReturn(Optional.of(analysisRun));
      given(analysisQueryService.getJobStatus(TASK_ID)).willReturn(JobStatus.ACTIVE);
    }

    @Nested
    class WhenReceivingCancelTask {
      @BeforeEach
      void setUp() {
        cut.cancelOrSave(cancelTaskRequest);
      }

      @Test
      void thenAnalysisIsStopped() {
        verify(analysisQueryService).stop();
      }
    }
  }

  @Nested
  class GivenTaskIsNotKnown {
    @BeforeEach
    void setUp() {
      given(analysisQueryService.getTaskDetails(TASK_ID)).willReturn(Optional.empty());
    }

    @Nested
    class WhenReceivingCancelTask {
      @BeforeEach
      void setUp() {
        cut.cancelOrSave(cancelTaskRequest);
      }

      @Test
      void thenAnalysisIsNotStoppedButSaved() {
        verify(analysisQueryService, never()).stop();
        verify(cancelTaskRepository).save(cancelTaskRequest);
      }
    }
  }

  @Test
  void doesCancelTask() {
    given(cancelTaskRepository.hasTaskId(TASK_ID)).willReturn(true);

    assertThat(cut.shouldBeCanceled(TASK_ID), is(true));
  }
}
